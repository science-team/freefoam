#!/bin/sh
#------------------------------------------------------------------------------
#               ______                _     ____          __  __
#              |  ____|             _| |_  / __ \   /\   |  \/  |
#              | |__ _ __ ___  ___ /     \| |  | | /  \  | \  / |
#              |  __| '__/ _ \/ _ ( (| |) ) |  | |/ /\ \ | |\/| |
#              | |  | | |  __/  __/\_   _/| |__| / ____ \| |  | |
#              |_|  |_|  \___|\___|  |_|   \____/_/    \_\_|  |_|
#
#                   FreeFOAM: The Cross-Platform CFD Toolkit
#
# Copyright (C) 2008-2012 Michael Wild <themiwi@users.sf.net>
#                         Gerber van der Graaf <gerber_graaf@users.sf.net>
#------------------------------------------------------------------------------
# License
#   This file is part of FreeFOAM.
#
#   FreeFOAM is free software: you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   FreeFOAM is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with FreeFOAM.  If not, see <http://www.gnu.org/licenses/>.
#
# Script
#     findBogusClassDoc
#
# Description
#     Finds files with wrong class documentation comments.
#
# Bugs
#     Reports quite a number of false positives (e.g. if the class name is
#     wrapped)
#------------------------------------------------------------------------------

usage() {
    exec 1>&2
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    cat<<USAGE
Usage: ${0##*/} [-h|-help] dir1 [dir2 .. dirN]

Finds files with wrong class documentation comments.

USAGE
    exit 1
}

# parse options
while [ "$#" -gt 0 ]
do
    case "$1" in
    -h | -help)
        usage
        ;;
    -*)
       usage "unknown option: '$*'"
       ;;
    *)
       break
       ;;
    esac
done

if [ $# -lt 1 ]; then
   usage "At least one directory name required"
fi

grep -rI -A1 --include '*.[CH]'  --exclude '*/lnInclude/*' '^Class\s*$' $@ | \
   awk -F- '/\.[CH]-/{print $1 $2;}' | \
   while read f c; do
      c=${c##*::}
      grep -q "^class $c\$" $f || echo $f
   done
