/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 1991-2010 OpenCFD Ltd.
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    chemkinToFoam

Description
    Converts CHEMKINIII thermodynamics and reaction data files into FOAM format

Usage

    - chemkinToFoam [OPTIONS] \<CHEMKINFile\> \<CHEMKINThermodynamicsFile\> \<FOAMChemistryFile\> \<FOAMThermodynamicsFile\>

    @param \<CHEMKINFile\> \n
    @todo Detailed description of argument.

    @param \<CHEMKINThermodynamicsFile\> \n
    @todo Detailed description of argument.

    @param \<FOAMChemistryFile\> \n
    @todo Detailed description of argument.

    @param \<FOAMThermodynamicsFile\> \n
    @todo Detailed description of argument.

    @param -case \<dir\>\n
    Case directory.

    @param -parallel \n
    Run in parallel.

    @param -help \n
    Display help message.

    @param -doc \n
    Display Doxygen API documentation page for this application.

    @param -srcDoc \n
    Display Doxygen source documentation page for this application.

\*---------------------------------------------------------------------------*/

#include <OpenFOAM/argList.H>
#include <reactionThermophysicalModels/chemkinReader.H>
#include <OpenFOAM/OFstream.H>

using namespace Foam;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// Main program:

int main(int argc, char *argv[])
{
    argList::validArgs.clear();
    argList::validArgs.append("CHEMKINFile");
    argList::validArgs.append("CHEMKINThermodynamicsFile");
    argList::validArgs.append("FOAMChemistryFile");
    argList::validArgs.append("FOAMThermodynamicsFile");
    argList args(argc, argv);

    fileName CHEMKINFileName(args.additionalArgs()[0]);
    fileName thermoFileName(args.additionalArgs()[1]);
    fileName FOAMChemistryFileName(args.additionalArgs()[2]);
    fileName FOAMThermodynamicsFileName(args.additionalArgs()[3]);

    chemkinReader cr(CHEMKINFileName, thermoFileName);

    OFstream reactionsFile(FOAMChemistryFileName);
    reactionsFile
        << "species" << cr.species() << ';' << endl << endl
        << "reactions" << cr.reactions() << ';' << endl;

    OFstream thermoFile(FOAMThermodynamicsFileName);
    thermoFile<< cr.speciesThermo() << endl;

    Info << "End\n" << endl;

    return 0;
}


// ************************ vim: set sw=4 sts=4 et: ************************ //
